<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    
    public function role()
    {
        return $this->hasOne(Role::class);
    }

    public function progresses()
    {
        return $this->hasMany(LearningProgress::class);
    }

    public function countComplete()
    {
        return $this->progresses->where('complete', true)->count();
    }

    public function completedLessons()
    {
        $progresses = $this->progresses->where('complete', true);
        $lessons = array();

        foreach ($progresses as $progress) {
            array_push($lessons, $progress->lesson);
        }
        return $lessons;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
