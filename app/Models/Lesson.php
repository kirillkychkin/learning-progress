<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    use HasFactory;

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
    
    public function progresses()
    {
        return $this->hasMany(LearningProgress::class);
    }
    
    public function countViews()
    {
        return $this->progresses->where('complete', true)->count();
    }
}
