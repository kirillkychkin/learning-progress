<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use Inertia\Inertia;

class CourseController extends Controller
{

    public function show()
    {   
        $course = Course::find(1);

        $lessons = $course->lessons;

        return Inertia::render('Contents', [
            'course' => $course,
            'lessons' => $lessons
        ]);
    }
}
