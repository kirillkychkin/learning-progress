<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Inertia\Inertia;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    public function showSorted()
    {   
        $pageSize = 25;
        $students = User::where('role_id',1)->orderBy('completed_lessons', 'desc')->paginate($pageSize);

        return Inertia::render('Students/List', [
            'students' => UserResource::collection($students)
        ]);
    }
    
    public function show()
    {   
        $pageSize = 25;
        $students = User::where('role_id',1)->paginate($pageSize);

        return Inertia::render('Students/List', [
            'students' => UserResource::collection($students)
        ]);
    }

    public function showCompletedLessons(Request $request)
    {
        $lessons = User::find($request->id)->completedLessons();

        return $lessons;
    }
}
