<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lesson;
use App\Models\LearningProgress;
use Inertia\Inertia;
use App\Http\Resources\StudentsFromProgressResource;

class LessonController extends Controller
{
    public function showSorted()
    {   
        $lessons = Lesson::orderBy('total_views', 'desc')->get();
 
        return Inertia::render('Lessons/ListTop', [
            'lessons' => $lessons
        ]);
    }

    public function show()
    {   
        $lessons = Lesson::all();

        return Inertia::render('Lessons/List', [
            'lessons' => $lessons
        ]);
    }

    public function showStudents(Request $request)
    {
        $pageSize = 25;
        $students = StudentsFromProgressResource::collection(LearningProgress::where('lesson_id', $request->id)->where('complete', true)->paginate($pageSize));

        return $students;
    }
}
