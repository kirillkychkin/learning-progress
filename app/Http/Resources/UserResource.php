<?php

namespace App\Http\Resources;
use App\Models\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'email' => $this->email,
            'completed_lessons' => $this->completed_lessons,
            'completion_percentage' => round($this->completed_lessons*100/27),
            'same_rank_first_pos' => User::where('role_id',1)->orderBy('completed_lessons', 'desc')->get()->search(function ($user, $key) 
            {
                return $user->completed_lessons == $this->completed_lessons;
            }),
            'students_same_rank' => User::where('completed_lessons', $this->completed_lessons)->count()
        ];
    }
}
