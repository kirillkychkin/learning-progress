<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;

class StudentsFromProgressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => User::find($this->user_id)->id,
            'name' => User::find($this->user_id)->name,
            'surname' => User::find($this->user_id)->surname,
            'email' => User::find($this->user_id)->email
        ];
    }
}
