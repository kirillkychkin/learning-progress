import { reactive } from 'vue'
import axios from 'axios'

const modalState = reactive({
    student: {
        completedLessons: []
    },
    lesson: {
        id: -1,
        usersCompleted: [],
        links: [],
        meta: {}
    },
    showModal: false
})

function openLessonsModal(id) {
    axios.get('/api/student/completed',{
        params: {
            id: id
        },
      },)
    .then(function (response) {
        modalState.student.completedLessons = response.data
        modalState.showModal = true
    })
    .catch(function (error) {
        // handle error
        console.log(error)
    })
}

function openStudentsModal(id) {
    modalState.lesson.id = 1
    axios.get('/api/lesson/completed',{
        params: {
            id: id
        },
      },)
    .then(function (response) {
        modalState.lesson.usersCompleted = response.data.data
        console.log(response.data.meta)
        modalState.lesson.links = response.data.meta.links
        modalState.showModal = true
    })
    .catch(function (error) {
        // handle error
        console.log(error)
    })
}

export function useModal() {
    return { 
        modalState,
        openLessonsModal,
        openStudentsModal
    }
}