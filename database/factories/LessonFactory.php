<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Lesson>
 */
class LessonFactory extends Factory
{
    protected $names = [
        'Как скачать UE4',
        'Интерфейс редактора',
        'Панель меню',
    ];

    protected $descriptions = [
        'В этом уроке рассказано как скачать UE4',
        'В этом уроке показан интерфейс редактора',
        'В этом уроке разобрана панель меню',
    ];

    protected $links = [
        'https://www.youtube.com/embed/qmkHkxKumj0',
        'https://www.youtube.com/embed/Sa8XsC_ALeY',
        'https://www.youtube.com/embed/VFTnhNz3H2k',
    ];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->names[random_int(0,2)],
            'description' => $this->descriptions[random_int(0,2)],
            'link' => $this->links[random_int(0,2)],
            'course_id' => 1,
        ];
    }
}
