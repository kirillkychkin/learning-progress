<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Lesson;
use Illuminate\Support\Facades\DB;

class LearningProgressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = User::where('role_id', 1)->get();
        $lessons = Lesson::where('course_id', 1)->get();

        foreach ($students as $student) 
        {
            foreach($lessons as $lesson)
            {
                /**
                 * В соответствии с условием ТЗ
                 * урок может быть пройденным если его
                 * номер до 20 включительно
                 */
                if($lesson->id > 20)
                {
                    DB::table('learning_progress')->insert([
                        'user_id' => $student->id,
                        'lesson_id' => $lesson->id,
                        'complete' => false,
                    ]);
                }

                else {
                    DB::table('learning_progress')->insert([
                        'user_id' => $student->id,
                        'lesson_id' => $lesson->id,
                        'complete' => fake()->boolean(),
                    ]);
                }
            }
        }
    }
}
