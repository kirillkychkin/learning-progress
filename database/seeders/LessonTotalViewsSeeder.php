<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Lesson;
use Illuminate\Support\Facades\DB;

class LessonTotalViewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lessons = Lesson::all();

        $lessons->each(function ($lesson) {
        
            $lesson->total_views = $lesson->countViews();
        
            $lesson->save();
        });
    }
}
