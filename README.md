# Тестовое задание №2 и №3 для productstar
## Описание

Проект выполнен на Laravel.

В сервисе реализованы возможности просмотра списка учеников, их рейтинга, уроков, а так же возможность посмотреть какие ученики завершили урок

## Используемые библиотеки

Использовано то, что было сказано использовать в ТЗ:
- Laravel

- Vue 3 Composition API

- Inertia

- Tailwind CSS

- Axios для реализации modal-ов. P.S.: Использую Inertia впервые в жизни и не понял есть ли возможность реализовать модальные окна без использования axios

- Сам проект инициализирован через Laravel Breeze

## На что обратить внимание
- Чтобы посмотреть список пройденных уроков нужно нажать непосредственно на имя пользователя (решил, что в ТЗ имеется ввиду это)
- На вкладках "уроки" и "уроки по просмотрам" можно посмотреть уроки на YouTube

Постарался использовать стилистику productstar:
- Задан цвет ProgressBar от Inertia как на сайте productstar
- Табы имеют те же стили
- Использованы логотип, favicon
- Элементы имеют схожие размеры и box-shadow

## Запуск проекта

Для запуска проекта необходимо клонировать репозиторий и запустить следующие команды:

- composer install
- npm i
- php artisan key:generate
- php artisan migrate:fresh --seed
- php artisan serve
- npm run dev
